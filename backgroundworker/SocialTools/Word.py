from ext.EmojiDict import EmojiDict

__author__ = 'guemues'


class Word:

    # Static variable
    emoji_dict = EmojiDict()

    def __init__(self, str_word):
        self.initialized = False

        str_word = str_word.replace('\n', '')
        word_ = Word.emoji_dict.extract_word(str_word)
        if word_ is not None:
            self.word = word_
            self.emotion = 0
            self.repatition = 1
            self.initialized = True

    def __hash__(self):
        return hash(self.word.encode('utf-8'))

    def __eq__(self, other):
        return self.get_word() == other.get_word()

    def __str__(self):
        return self.word.encode('utf-8')

    def increase(self):
        self.repatition += 1

    def get_word(self):
        return self.word

    def is_initialized(self):
        return self.initialized

    def is_emoji(self):
        return Word.emoji_dict.extract_word(self.word)

    @staticmethod
    def extract_word(str_word):
        return Word.emoji_dict.extract_word(str_word)

    @staticmethod
    def turkish_utf8(word):
        """

        :rtype : string
        """
        return word.replace("İ", "i").replace("İ", "i").replace("Ç","c").replace("Ş","s").replace("Ğ","g").lower().replace("ğ","g").replace("ş","s").replace("ç","c").replace("ı","i").replace("ü","u").replace("ö","o").replace("ç","c").replace("\"","").replace("'","");

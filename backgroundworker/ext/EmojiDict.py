__author__ = 'orcungumus'
__version__ = '0.0.0'

import os
import pandas as pd

class EmojiDict(object):

    def __init__(self):
        file_path = os.path.dirname(os.path.abspath(__file__))
        emoji_key = pd.read_csv(file_path + '/data/' + 'emoji_table.txt', encoding='utf-8', index_col=0)
        emoji_key['count'] = 0
        emoji_dict = emoji_key['count'].to_dict()
        emoji_dict_total = emoji_key['count'].to_dict()

        self.dict = emoji_dict
        self.dict_total = emoji_dict_total
        self.emoji_list = emoji_dict.keys()
        self.baskets = []

    def add_emoji_count(self, text):
        emoji_basket = []
        for emoji in self.emoji_list:

            if emoji in text:
                self.dict[emoji] += 1
                emoji_basket.append(emoji)

            self.dict_total[emoji]  += text.count(emoji)

        self.baskets.append(emoji_basket)
        return

    def create_csv(self, file='emoji_out.csv', total=False):

        emoji_df_total = self.dict_total.items()
        emoji_df_unique = self.dict.items()

        emoji_count = pd.DataFrame(emoji_df_total, columns=['emoji', 'total'])
        emoji_unique = pd.DataFrame(emoji_df_unique, columns=['emoji', 'unique'])

        emoji_df = pd.merge(left=emoji_count, right=emoji_unique, on=['emoji'])

        emoji_df.sort("unique", ascending=False)

        with open(file, 'w') as f:
            emoji_df.to_csv(f, sep=',', index = False, encoding='utf-8')
        return

    def clear(self):

        for emoji in self.dict.keys():
            self.dict[emoji] = 0
        return

    def __str__(self):
        return str(self.dict)

    def extract_word(self,word):
        u_word = word.encode('unicode_escape')
        if '\\U' in u_word:
            for splitted in u_word.split('\\'):
                temp_word = ('\\' + splitted)
                if len(temp_word) > 3:
                    temp_word = temp_word.decode(encoding='unicode_escape')
                    if temp_word in self.emoji_list:
                        u_word = u_word.replace(temp_word.encode(encoding='unicode_escape'), '')

        utfWord = u_word.decode(encoding='unicode_escape')
        if len(utfWord) > 2:
            return utfWord
        else:
            return None

    def contains_emoji(self,word):
        u_word = word.encode('unicode_escape')
        if '\\U' in u_word:
            for splitted in u_word.split('\\'):
                temp_word = ('\\' + splitted)
                if len(temp_word) > 3:
                    temp_word = temp_word.decode(encoding='unicode_escape')
                    if temp_word in self.emoji_list:
                        return True





import collections
import random
import math
import community as comm
import networkx as nx
import networkx.algorithms.cluster as clustering

from .Community import Community
from .Network import Network


class MultiNetwork(Network):

    def __init__(self, networkx, community, positions=None):
        super().__init__(networkx=networkx, community=community, positions=positions)
        self.networks = []
        self.generate_sub_networks()

    def generate_sub_networks(self):

        communities = {}
        instausers = {instauser.uid: instauser for instauser in self.instausers}
        partitions_ = comm.best_partition(self._networkx)

        for partition in set(val for val in partitions_.values()):
            sub_graph_nodes = []
            for key, partition_ in partitions_.items():
                if partition == partition_:

                    sub_graph_nodes.append(key)

                    if partition_ in communities.keys():
                        communities[partition].add_instauser(
                            instausers[key]
                        )
                    else:

                        communities[partition] = Community(

                            from_db=False,
                            params={
                                'source_instauser_uid': self.source_instauser_uid
                            },
                            instauser=self.instauser,
                            instausers=[instausers[key]]

                        )
            sub_graph = self.networkx.subgraph(sub_graph_nodes)

            coef = clustering.average_clustering(G=sub_graph)

            communities[partition].set_clustering_coef(coef)

            self.networks.append(Network(networkx=sub_graph,
                                         average_clustering=coef,
                                         community=communities[partition]))

    @property
    def communities(self):
        for network in self.networks:
            yield network.community

    @property
    def get_sub_community_uid(self, instauser_uid):
        for network in self.networks:
            if network.community.contains_instauser(instauser_uid):
                return network.community_uid

    @property
    def json(self):
        graph_json = dict()

        nodes = []
        edges = []
        colors = {}

        for sub_networks in self.networks:
            color_collection = collections.namedtuple('RGB', 'red, green, blue')
            color = color_collection(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
            colors[sub_networks.community_uid] = color

        for i in self._networkx.nodes_iter(data=True):

            # i = (id, {username: , size: }

            node_community = self.get_community_uid_of_instauser(i[0])

            node = dict()

            node['x'] = self._positions[i[0]][0] * 2
            node['y'] = self._positions[i[0]][1] * 2
            node['label'] = i[1]['username']
            node['id'] = i[0]
            if i[1]['size'] is not None:
                node['size'] = math.log10(math.fabs(i[1]['size']) + 2)

            node['color'] = "rgb({0}, {1}, {2})".format(colors[node_community][0],
                                                        colors[node_community][1],
                                                        colors[node_community][2])

            node['attributes'] = {'community': node_community}

            nodes.append(node)

        index = 0
        for i in self._networkx.edges_iter(data=False):
            edge = dict()

            edge['id'] = index
            index += 1

            edge['source'] = i[0]
            edge['target'] = i[1]
            edges.append(edge)

        graph_json['edges'] = edges
        graph_json['nodes'] = nodes

        return graph_json

    def get_community_uid_of_instauser(self, instauser_uid):
        for community in self.communities:
            if community.contains_instauser(instauser_uid):
                return community.uid

        return None

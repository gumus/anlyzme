from .InstaGroup import InstaGroup
from .InstaRelation import InstaRelation
from Contollers.SocialBase import SocialBase

__author__ = 'guemues'


class Follows(InstaGroup):

    def __init__(self, from_db, instauser, instausers, params=None):
        """

        :param instauser: InstaUser object of the person who follows by
        :param instausers:  List of InstaUser
        :param params:
        """
        super().__init__(instausers=instausers, params=params)
        self._instauser = instauser
        self.from_db = from_db

    @property
    def instauser(self):
        return self._instauser

    @property
    def source_instauser_uid(self):
        return self._instauser.instauser_uid

    @property
    def source_instauser_username(self):
        return self._instauser.username

    @property
    def len(self):
        return len(self._instausers)

    @property
    def instausers(self):
        return self._instausers

    def get_instarelations(self):
        """
            Return the correlated list of InstaRelations

        :rtype : list of InstaRelations
        """
        relations_ = []

        for instauser in self._instausers:
            relations_.append(InstaRelation(params={'source_uid': self._instauser.instauser_uid, 'target_uid': instauser.instauser_uid}))

        return relations_

from .InstaGroup import InstaGroup

__author__ = 'guemues'


class Followers(InstaGroup):

    def __init__(self, instauser=None, instausers=None, params=None):
        """

        :param instauser: InstaUser object of the person who followed
        :param instausers:  List of InstaUser
        :param params:
        """
        super().__init__(instausers=instausers, params=params)
        self._instauser = instauser

    @property
    def instauser_uid(self):
        return self._instauser

    def get_relations(self):
        """
            Return the correlated list of InstaRelations

        :rtype : list of InstaRelations
        """
        pass
from enum import Enum

from .SocialBaseObject import SocialBaseObject
from .CommunityRelation import CommunityRelation

__author__ = 'guemues'


class CommunityType(Enum):
    instauser = 1
    community = 2


class Community(SocialBaseObject):

    db_VARS = ['community_uid', 'source_instauser_uid', 'source_community_uid', 'clustering_coef', 's3location']

    TABLE_NAME = 'communities'
    UNIQUE_ID = 'community_uid'

    def __init__(self,
                 from_db=False,
                 instauser=None,
                 instausers=[],
                 community_type=CommunityType.community,
                 vars__=None,
                 params=None):

        if not vars__:
            vars__ = []
        vars_ = vars__ + Community.db_VARS
        super().__init__(vars__=vars_, params=params)

        self._instausers = instausers
        self._instauser = instauser
        self.from_db = from_db
        self._community_type = community_type
        if self.community_type is CommunityType.instauser:
            self._info['s3location'] = str(self.source_instauser_uid) + '/'

    def add_instauser(self, instauser):
        self._instausers.append(instauser)

    def set_clustering_coef(self, coef):
        self._info['clustering_coef'] = coef

    @property
    def community_type(self):
        return self._community_type

    @property
    def instausers(self):
        return self._instausers

    @property
    def source_instauser(self):
        return self._instauser

    @property
    def source_instauser_uid(self):
        return self._instauser.uid

    @property
    def community_relations(self):
        """
            Return the correlated list of InstaRelations

        :rtype : list of InstaRelations
        """
        if 'community_uid' not in self._info.keys():
            print('Community not inserted yet')
            return None

        relations_ = []

        for instauser in self._instausers:
            relations_.append(CommunityRelation(
                params={'instauser_uid': instauser.instauser_uid,
                        'community_uid': self.community_uid}))
        return relations_

    def contains_instauser(self, instauser_uid):
        for instauser in self.instausers:
            if instauser_uid == instauser.uid:
                return True
        return False

    def set_source(self, source_community_uid, source_s3location):
        self._info['s3location'] = source_s3location + str(source_community_uid) + '/'
        # Not good style (usage of protected info)

        self._info['source_community_uid'] = source_community_uid
        # Not good style (usage of protected info)


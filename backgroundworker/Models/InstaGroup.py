__author__ = 'guemues'


class InstaGroup(object):

    def __init__(self, instausers=None, params=None):
        """
            This is a user group class
        :param instausers: List of InstaUser
        :param params:
        """

        self._instausers = instausers

        pass

    @property
    def instausers(self):
        return self._instausers

    def get_user(self, uid):
        if uid in self._instausers:
            return self._instausers[uid]
        else:
            return None

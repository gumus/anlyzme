from .SocialBaseObject import SocialBaseObject

__author__ = 'guemues'


class CommunityRelation(SocialBaseObject):

    db_VARS = ['community_relation_uid', 'instauser_uid', 'community_uid']

    TABLE_NAME = 'community_relations'
    UNIQUE_ID = 'community_relation_uid'

    def __init__(self, vars__=None, params=None):
        if not vars__:
            vars__ = []
        vars_ = vars__ + CommunityRelation.db_VARS
        super().__init__(vars__=vars_, params=params)

        # if all(item in params for item in
        # [item for item in InstaUser.db_VARS
        #  if item not in InstaUser.DETAILED_VARS]):



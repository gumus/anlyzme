from .SocialBaseObject import SocialBaseObject

__author__ = 'guemues'


class InstaRelation(SocialBaseObject):

    db_VARS = ['relation_uid', 'source_uid', 'target_uid' ]

    TABLE_NAME = 'instarelations'
    UNIQUE_ID = 'relation_uid'

    def __init__(self, vars__=None, params=None):
        if not vars__:
            vars__ = []
        vars_ = vars__ + InstaRelation.db_VARS
        super().__init__(vars__=vars_, params=params)

        # if all(item in params for item in
        # [item for item in InstaUser.db_VARS
        #  if item not in InstaUser.DETAILED_VARS]):



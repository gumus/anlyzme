
__author__ = 'guemues'


class InstaUserCommunityGroup(object):

    def __init__(self, instauser, instauser_communities):
        """

        :param instauser: InstaUser object of the person who follows by
        :param instausers:  List of InstaUser
        :param params:
        """
        self._instauser_communities = instauser_communities
        self._instauser = instauser

    @property
    def source_instauser_uid(self):
        return self._instauser.instauser_uid

    @property
    def instauser_communities(self):
        return self._instauser_communities


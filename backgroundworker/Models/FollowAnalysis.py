from .Analysis import Analysis

__author__ = 'guemues'


class FollowAnalysis(Analysis):

    db_VARS = ['instauser_uid', 'limit']
    IN_PROCESS_KEY = 'load_follows_follows'

    def __init__(self, vars__=None, params=None):
        if not vars__:
            vars__ = []
        vars_ = vars__ + FollowAnalysis.db_VARS
        super().__init__(vars__=vars_, params=params)

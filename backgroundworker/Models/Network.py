import collections
import random
import math
import networkx as nx
from enum import Enum

import networkx.algorithms.cluster as clustering

__author__ = 'guemues'


class NetworkType(Enum):
    instauser = 1
    community = 2


class Network(object):

    def __init__(self, networkx, community, average_clustering=None, positions=None):
        self._networkx = networkx
        self._positions = positions
        self._community = community
        self._average_clustering = average_clustering

        if not average_clustering:
            self._average_clustering = clustering.average_clustering(G=networkx)

        if not positions:
            self._positions = nx.spring_layout(networkx, scale=20)

    @property
    def networkx(self):
        return self._networkx

    @property
    def positions(self):
        return self._positions

    @property
    def community(self):
        return self._community

    @property
    def community_uid(self):
        return self._community.uid

    @property
    def instausers(self):
        return self._community.instausers

    @property
    def instauser(self):
        return self._community.source_instauser

    @property
    def source_instauser_uid(self):
        return self._community.source_instauser.uid

    @property
    def source_community_uid(self):
        return self._community.source_community_uid

    @property
    def name(self):
        if self.network_type is NetworkType.instauser:
            return str(self.community_uid) + '.json'
        elif self.source_community_uid:
            return str(self.community_uid) + '.json'
        else:
            raise NotImplementedError

    @property
    def s3file_location(self):
        return self.community.s3location + str(self.community_uid) + '.json'

    @property
    def s3folder_location(self):
        return self.community.s3location

    @property
    def network_type(self):
        if hasattr(self, 'source_community_uid') and self.source_community_uid:
            return NetworkType.community
        else:
            return NetworkType.instauser

    @property
    def json(self):
        graph_json = dict()

        nodes = []
        edges = []

        Color = collections.namedtuple('RGB', 'red, green, blue')
        color = Color(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))

        for i in self._networkx.nodes_iter(data=True):
            node = dict()
            attributes = dict()
            node['x'] = self._positions[i[0]][0] * 2
            node['y'] = self._positions[i[0]][1] * 2
            node['label'] = i[1]['username']
            node['id'] = i[0]

            if i[1]['size'] is not None:
                node['size'] = math.log10(math.fabs(i[1]['size']) + 2)

            node['attributes'] = attributes

            node['color'] = "rgb({0}, {1}, {2})".format(color[0],
                                                        color[1],
                                                        color[2])
            nodes.append(node)

        index = 0
        for i in self._networkx.edges_iter(data=False):
            edge = dict()

            edge['id'] = index
            index += 1

            edge['source'] = i[0]
            edge['target'] = i[1]
            edges.append(edge)

        graph_json['edges'] = edges
        graph_json['nodes'] = nodes

        return graph_json



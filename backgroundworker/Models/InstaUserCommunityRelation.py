from .SocialBaseObject import SocialBaseObject

__author__ = 'guemues'


class InstaUserCommunityRelation(SocialBaseObject):

    db_VARS = ['instauser_community_relation_uid', 'instauser_uid', 'instauser_community_uid']

    TABLE_NAME = 'instauser_community_relations'
    UNIQUE_ID = 'instauser_community_relation_uid'

    def __init__(self, vars__=None, params=None):
        if not vars__:
            vars__ = []
        vars_ = vars__ + InstaUserCommunityRelation.db_VARS
        super().__init__(vars__=vars_, params=params)

        # if all(item in params for item in
        # [item for item in InstaUser.db_VARS
        #  if item not in InstaUser.DETAILED_VARS]):



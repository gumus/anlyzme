from .SocialBaseObject import SocialBaseObject
from .InstaUserCommunityRelation import InstaUserCommunityRelation

__author__ = 'guemues'


class InstaUserCommunity(SocialBaseObject):

    db_VARS = ['instauser_community_uid', 'source_instauser_uid', 'clustering_coef']

    TABLE_NAME = "instauser_communities"
    UNIQUE_ID = "instauser_community_uid"

    def __init__(self, from_db=False, network=None, instauser=None, instausers=None, vars__=None, params=None):
        if not vars__:
            vars__ = []
        vars_ = vars__ + InstaUserCommunity.db_VARS
        super().__init__(vars__=vars_, params=params)
        self._instausers = instausers
        self._instauser = instauser
        self._network = network
        self.from_db = from_db

    def add_instauser(self, instauser):
        self._instausers.append(instauser)

    def set_clustering_coef(self, double):
        self._info['clustering_coef'] = double

    @property
    def instauser_community(self):
        return self._instauser_community

    @property
    def instauser_community_relations(self):
        """
            Return the correlated list of InstaRelations

        :rtype : list of InstaRelations
        """
        if 'instauser_community_uid' not in self._info.keys():
            print('Community not inserted yet')
            return None

        relations_ = []
        print(self._instausers)
        for instauser in self._instausers:
            relations_.append(InstaUserCommunityRelation(
                params={'instauser_uid': instauser.instauser_uid,
                        'instauser_community_uid': self.instauser_community_uid}))
        return relations_

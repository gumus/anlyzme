from abc import ABCMeta

from datetime import datetime

__author__ = 'guemues'


class SocialBaseObject(object):
    __metaclass__ = ABCMeta

    VARS = ['created_at']
    # All names of variable - same as the database record

    DETAILED_VARS = []
    # The subset of VARS to make the instance detailed or not.
    # For example: for InstaUser Class follow, follower and media count numbers are detailed variable
    # and can be NULL in database

    EXTRA_IDS = []
    # A table may have more then one UNIQUE ID.

    EXTRA_VARS = []

    CHARS = []
    # Some vars are characters in database, so before insert it is must to check them to make them UTF8

    TABLE_NAME = ""
    # Table name of the SocialTools Base Object

    UNIQUE_ID = ""
    # The UNIQUE ID

    def __init__(self, is_exists_in_db=True, from_db=True, vars__=None, params=None):
        if not vars__:
            vars__ = []

        self.vars = []
        self.vars += SocialBaseObject.VARS + vars__

        # The info dictionary holds all database attributes inside
        self._info = dict()

        # SocialBaseObject responsible for database communications
        self._set(params)

        # SocialBase object can be from databes or not
        self.from_db = from_db

        self.is_exists_in_db = is_exists_in_db

        # Created at
        if 'created_at' in self.vars:
            self._info['created_at'] = str(datetime.now())

        if 'updated_at' in self.vars:
            self._info['updated_at'] = str(datetime.now())

    # __init__ end

    def __getattr__(self, name):
        if (name in self.vars) and (name in self._info):
            return self._info[name]
        return None
    # __getattr__ end

    def __hash__(self):
        return self.uid

    def __eq__(self, other):
        if isinstance(other,  int):
            return other == self.uid
        else:
            return self.uid == other.uid

    def _set(self, params):
        if not params:
            return

        for key, value in params.items():
            if key in self.vars:
                if key == self.UNIQUE_ID:
                    value = int(float(value))
                self._info[key] = value
        return
    # _set end

    def have_uid(self):
        return self.UNIQUE_ID in self._info.keys()

    @property
    def info(self):
        return self._info

    @property
    def uid(self):

        if len(self.EXTRA_IDS):
            raise NotImplementedError

        return self._info[self.UNIQUE_ID]

    @uid.setter
    def uid(self, value):
        if len(self.EXTRA_IDS):
            print("Error")
        self._info[self.UNIQUE_ID] = value
    # insert end

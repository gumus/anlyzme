from abc import ABCMeta

from .SocialBaseObject import SocialBaseObject
from Contollers.SocialBase import SocialBase

__author__ = 'guemues'


class Analysis(SocialBaseObject):
    _metaclass__ = ABCMeta

    db_VARS = ['analysis_uid',
               'in_process',
               'finished',
               'crushed',
               'type',
               'crushed_msg',
               'instauser_uid',
               'created_at',
               'updated_at']

    UNIQUE_ID = 'analysis_uid'
    TABLE_NAME = 'analysis'

    EXTRA_VARS = ['access_token']

    def __init__(self, vars__=None, params=None):
        if not vars__:
            vars__ = []
        vars_ = vars__ + Analysis.db_VARS + Analysis.EXTRA_VARS
        super().__init__(vars__=vars_, params=params)

    def finished(self, success=1):
        query = "UPDATE analysis SET finished = %s, in_process = 100 WHERE uid = %s" % (str(success), str(self.uid))
        SocialBase.modify(query)
    # finished end

    def update_in_process(self):
        self.database.conn.execute(
            "UPDATE analysis SET in_process = %s WHERE uid = %s " % (self.in_processes[self.in_process_key]
                                                                     , self.uid))
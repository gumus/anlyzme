from .SocialBaseObject import SocialBaseObject

__author__ = 'guemues'


class InstaUser(SocialBaseObject):

    db_VARS = ['instauser_uid', 'username', 'profile_picture', 'full_name', 'gender', 'is_public', 'total_followers',
               'total_media', 'total_follows', 'updated_at',
               'is_loaded', 'total_media', 'total_followers', 'total_follows', 'follows_gr_uid']

    db_DETAILED_VARS = ['total_media', 'total_followers', 'total_follows']
    TABLE_NAME = "instausers"
    UNIQUE_ID = "instauser_uid"

    CHARS = ['full_name']

    def __init__(self, is_exists_in_db=True, from_db=True, vars__=None, params=None):
        """
        """
        if not vars__:
            vars__ = []
        vars_ = vars__ + InstaUser.db_VARS

        super().__init__(is_exists_in_db=is_exists_in_db, from_db=from_db, vars__=vars_, params=params)

    def __str__(self):
        return str(self._info[self.UNIQUE_ID])

    @property
    def is_detailed(self):
        is_detailed = True

        for detailed_var in self.db_DETAILED_VARS:
            if is_detailed:
                is_detailed = (detailed_var in self.info) and self.info[detailed_var]

        return is_detailed


    @property
    def is_instance(self):
        """
            Is this only contain id?
        :return:
        """
        return 'username' not in self._info.keys()

    @staticmethod
    def instance(instauser_uid):
        return InstaUser(params={InstaUser.UNIQUE_ID: instauser_uid})

    @staticmethod
    def change_to_own(user):
        instauser = None

        if user:
            params = {}
            if hasattr(user, 'id'):
                params['instauser_uid'] = user.id
            if hasattr(user, 'username'):
                params['username'] = user.username
            if hasattr(user, 'full_name'):
                params['full_name'] = user.full_name
            if hasattr(user, 'profile_picture'):
                params['profile_picture'] = user.profile_picture
            if hasattr(user, 'bio'):
                params['bio'] = user.bio
            if hasattr(user, 'counts') and 'follows' in user.counts:
                params['total_follows'] = user.counts['follows']
            if hasattr(user, 'counts') and 'followed_by' in user.counts:
                params['total_followers'] = user.counts['followed_by']
            if hasattr(user, 'counts') and 'media' in user.counts:
                params['total_media'] = user.counts['media']

            instauser = InstaUser(from_db=False, params=params)

        else:
            print("User is private")

        return instauser

from Contollers.Managers.Adapters.InstaUserDatabaseAdapter import InstaUserDatabaseAdapter
from Contollers.Managers.ApiGetters.InstaUserApiGetter import InstaUserApiGetter

__author__ = 'guemues'


class InstaUserManager(object):

    def __init__(self, database, insta_connection):
        self._instauser_database_adapter = InstaUserDatabaseAdapter(database=database)
        self._instauser_api_getter = InstaUserApiGetter(instaram_connection=insta_connection)

    def get_and_insert(self, instauser_uid):
        instauser = self.get(instauser_uid=instauser_uid)
        if not instauser.is_exists_in_db:
            self._instauser_database_adapter.insert(instauser)
        else:

            self._instauser_database_adapter.update(instauser, set_values=instauser.info)

        return instauser

    def insert(self, instauser):
        self._instauser_database_adapter.insert(instauser)

    def get(self, instauser_uid):
        instauser = None

        instauser_db = self._instauser_database_adapter.get(instauser_uid=instauser_uid)

        if not instauser_db:
            follows_api = self._instauser_api_getter.get(instauser_uid=instauser_uid)
            follows_api.is_exists_in_db = False
            instauser = follows_api
        elif not instauser_db.is_detailed:
            follows_api = self._instauser_api_getter.get(instauser_uid=instauser_uid)
            follows_api.is_exists_in_db = True

            instauser = follows_api
        else:
            instauser = instauser_db

        return instauser

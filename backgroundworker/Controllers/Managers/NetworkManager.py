from .Adapters.NetworkS3Adapter import NetworkS3ObjectAdapter
from .Adapters.CommunityDatabaseAdapter import CommunityDatabaseAdapter

from Models.Network import NetworkType
from Models.MultiNetwork import MultiNetwork
from Models.Community import Community
from Models.InstaUser import InstaUser

__author__ = 'guemues'


class NetworkManager(object):

    def __init__(self, s3client, database):
        self._communitiy_database_adapter = CommunityDatabaseAdapter(database=database)
        self._networkS3Adapter = NetworkS3ObjectAdapter(s3client=s3client)

    def get(self, community_uid):
        community = self._communitiy_database_adapter.get_without_instausers(community_uid)

        if not community:
            raise NotImplementedError

        network = self._networkS3Adapter.get_network(community)

        for node in network.networkx.nodes():
            community.add_instauser(InstaUser.instance(node))

        return network

    def insert(self, network):

        if not isinstance(network, MultiNetwork):
            raise NotImplementedError

        #  If there exist previous same source community_uid erase them
        print(network.network_type)
        if network.network_type is NetworkType.community:
            self._communitiy_database_adapter.delete(
                socialbaseobject=Community(),
                delete_conditions={
                    'source_community_uid': network.community_uid
                }
            )
            #  Delete all community graphs
            self._networkS3Adapter.delete_all(network)
        elif network.network_type is NetworkType.instauser:
            self._communitiy_database_adapter.delete(
                socialbaseobject=Community(),
                delete_conditions={
                    'source_instauser_uid': network.source_instauser_uid
                }
            )
            #  Delete all instauser graphs
            self._networkS3Adapter.delete_all(network)

        # If this is a community network it must have a community in database
        if network.network_type is NetworkType.instauser:
            self._communitiy_database_adapter.insert(community=network.community)

        for community in network.communities:
            community.set_source(source_community_uid=network.community_uid,
                                 source_s3location=network.s3folder_location)
            self._communitiy_database_adapter.insert(community=community)

        # Insert instauser graph
        self._networkS3Adapter.insert(network)

        # Insert community graphs
        for sub_network in network.networks:
            self._networkS3Adapter.insert(sub_network)


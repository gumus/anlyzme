from Contollers.Managers.Adapters.CommunityDatabaseAdapter import CommunityDatabaseAdapter
from .Adapters.SocialBaseObjectDatabaseAdapter import SocialBaseObjectDatabaseAdapter
from .Adapters.InstaRelationDatabaseAdapter import InstaRelationDatabaseAdapter

from Models.Network import Network
import networkx.algorithms.cluster as clustering

import networkx as nx

__author__ = 'guemues'


class CommunityManager(SocialBaseObjectDatabaseAdapter):

    def __init__(self, database, s3client):
        super().__init__(database=database)

        self._instarelation_database_adapter = InstaRelationDatabaseAdapter(database=database)
        self._communitiy_database_adapter = CommunityDatabaseAdapter(database=database)

    def get(self, community_uid):
        return self._communitiy_database_adapter.get(community_uid=community_uid)

    def create_network(self, community, all_relations=None):
        i = 0
        global relations
        data_retrieval = False

        if not all_relations:
            data_retrieval = True
            all_relations = []

        networkx_ = nx.Graph()

        for instauser in community.instausers:

            if data_retrieval:
                relations = self._instarelation_database_adapter.get(source_instauser_uid=instauser.uid)
                all_relations += relations
            networkx_.add_node(instauser.uid, {'username': instauser.username, 'size': len(relations)})

        for relation in all_relations:
            if networkx_.has_node(relation.target_uid):
                networkx_.add_edge(relation.source_uid, relation.target_uid)

        pos = nx.spring_layout(networkx_, scale=20)

        community.set_clustering_coef(
            coef=clustering.average_clustering(G=networkx_)
        )

        return Network(community=community, networkx=networkx_, positions=pos)








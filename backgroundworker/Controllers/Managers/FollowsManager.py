from Contollers.Managers.Adapters.FollowsDatabaseAdapter import FollowsDatabaseAdapter
from Contollers.Managers.ApiGetters.InstaRelationApiGetter import InstaRelationApiGetter
from .InstaUserManager import InstaUserManager

__author__ = 'guemues'


class FollowsManager(object):

    def __init__(self, database, insta_connection):
        self._instauser_manager = InstaUserManager(database=database, insta_connection=insta_connection)
        self._follows_database_adapter = FollowsDatabaseAdapter(database=database)
        self._instarelation_api_getter = InstaRelationApiGetter(instaram_connection=insta_connection)

    def get_and_insert(self, instauser_uid):
        follows = self.get_follows(source_instauser_uid=instauser_uid)
        if not follows.from_db:
            self.insert(follows=follows)
        else:
            print('0', end='', flush=True)
        return follows

    def insert(self, follows):
        """
        This metdod is inserting Follows object to database if it is full and do not exists in the database
        :param follows:
        """
        self._follows_database_adapter.insert(follows=follows)

    def get_follows(self, source_instauser_uid):
        follows = None

        instauser = self._instauser_manager.get_and_insert(source_instauser_uid)

        follows_db = self._follows_database_adapter.get(source_instauser_uid=source_instauser_uid)

        if not follows_db:
            follows_api = self._instarelation_api_getter.follows(source_instauser=instauser)
            follows = follows_api
        else:
            follows = follows_db
        return follows

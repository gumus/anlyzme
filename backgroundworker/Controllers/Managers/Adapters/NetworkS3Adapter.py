from .S3ObjectAdapter import S3ObjectAdapter
from Models.Network import Network
from Models.Network import NetworkType

import json
import os
import networkx as nx
import boto3

__author__ = 'guemues'


class NetworkS3ObjectAdapter(S3ObjectAdapter):

    def __init__(self, s3client):
        super().__init__(s3client)

    def get_network(self, community):
        networkx = nx.Graph()

        file_name = 'temps/' + str(community.uid) + '.json'

        key = str(community.s3location) + str(community.uid) + '.json'

        self._s3client.download_file(
            Bucket=self.GRAPH_BUCKET,
            Key=key,
            Filename=file_name
        )

        networkx_json = json.load(open(file_name))

        for node in networkx_json['nodes']:
            networkx.add_node(node['id'], attr_dict={'username': node['label'], 'size': node['size']})

        for edge in networkx_json['edges']:
            networkx.add_edge(u=edge['source'], v=edge['target'])

        return Network(networkx=networkx, community=community)

    def insert(self, network):
        file_loc = os.path.join('temps', network.name)
        object_key = network.s3file_location

        with open(file_loc, 'w') as fp:
            json.dump(network.json, fp)

        self._s3client.upload_file(Bucket=self.GRAPH_BUCKET,
                                   Filename=file_loc,
                                   Key=object_key)

        os.remove(file_loc)

    def delete_all(self, network):
        if network.network_type is NetworkType.instauser:
            location = network.s3folder_location
        else:
            location = network.s3folder_location + str(network.community_uid) + '/'

        objects_ = self._s3client.list_objects(Bucket=self.GRAPH_BUCKET,
                                               Prefix=location)
        if 'Contents' in objects_:
            for object_ in objects_['Contents']:
                self._s3client.delete_object(Bucket=self.GRAPH_BUCKET, Key=object_['Key'])

    def delete_graph(self, graph_type, graph_id):
        self._s3client.delete_object(Bucket=self.GRAPH_BUCKET, Key=graph_type + '-' + graph_id)


import boto3


class S3ObjectAdapter(object):

    GRAPH_BUCKET = 'analyzeforme-graphs'

    def __init__(self, s3client):
        self._s3client = s3client

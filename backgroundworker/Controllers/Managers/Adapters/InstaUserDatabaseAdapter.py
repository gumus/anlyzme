from Contollers.Managers.Adapters.SocialBaseObjectDatabaseAdapter import SocialBaseObjectDatabaseAdapter
from Models.InstaUser import InstaUser

__author__ = 'guemues'


class InstaUserDatabaseAdapter(SocialBaseObjectDatabaseAdapter):

    def __init__(self, database):
        super().__init__(database=database)

    def get(self, instauser_uid):
        instausers = []

        query = "SELECT *  FROM instausers WHERE instausers.instauser_uid = %s" % instauser_uid

        row = self._database.select_row(query)
        if not row:
            return None
        return InstaUser(is_exists_in_db=True, from_db=True, params=row)

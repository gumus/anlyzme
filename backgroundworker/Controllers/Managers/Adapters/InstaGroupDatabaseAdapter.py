from Contollers.Managers.Adapters import SocialBaseObjectDatabaseAdapter

__author__ = 'guemues'


class InstaGroupDatabaseAdapter(SocialBaseObjectDatabaseAdapter):

    def get(self, socialbaseobject):
        raise NotImplementedError

    def __init__(self, database):
        super().__init__(database)

    def insert(self, instagroup):
        for instauser in instagroup.instausers:
            super().insert(instauser)

from .InstaUserDatabaseAdapter import InstaUserDatabaseAdapter
from .SocialBaseObjectDatabaseAdapter import SocialBaseObjectDatabaseAdapter


from Models.InstaUser import InstaUser
from Models.Community import Community

__author__ = 'guemues'


class CommunityDatabaseAdapter(SocialBaseObjectDatabaseAdapter):
    def __init__(self, database):
        super().__init__(database=database)
        self._instauser_database_getter = InstaUserDatabaseAdapter(database=database)

    def insert(self, community):
        super().insert(community)
        for community_relation in community.community_relations:
            super().insert(community_relation)

    def get_parent_communities(self, community_uid):
        source_community_uid = True

        while source_community_uid:
            query = "SELECT source_community_uid FROM communities WHERE community_uid = %s" % (community_uid)
            self._database.select_row(query)


    def get_without_instausers(self, community_uid):
        query = "SELECT communities.* FROM communities " \
                "WHERE communities.community_uid = %s" % community_uid

        row = self._database.select_row(query)

        if not row:
            return None

        instauser = self._instauser_database_getter.get(instauser_uid=row['source_instauser_uid'])

        community = Community(from_db=True,
                              params=row,
                              instauser=instauser)

        return community

    def get_with_instausers(self, community_uid):

        community = None
        instauser = None

        query = "SELECT communities.*,instausers.* FROM communities " \
                "INNER JOIN community_relations ON " \
                "community_relations.community_uid = communities.community_uid " \
                "INNER JOIN instausers ON community_relations.instauser_uid = instausers.instauser_uid " \
                "WHERE communities.community_uid = %s" % community_uid

        rows = self._database.select(query)

        print(query)

        if len(rows) < 1:
            return None

        for row in rows:

            if not instauser or not community:
                instauser = self._instauser_database_getter.get(instauser_uid=row['source_instauser_uid'])

                community = Community(from_db=True,
                                      params=row,
                                      instauser=instauser)

            community.add_instauser(InstaUser(params=row))

        return community




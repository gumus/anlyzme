from Contollers.Managers.Adapters.InstaUserDatabaseAdapter import InstaUserDatabaseAdapter
from Contollers.Managers.Adapters.SocialBaseObjectDatabaseAdapter import SocialBaseObjectDatabaseAdapter
from Models.Follows import Follows
from Models.InstaUser import InstaUser

__author__ = 'guemues'


class FollowsDatabaseAdapter(SocialBaseObjectDatabaseAdapter):

    def __init__(self, database):
        super().__init__(database=database)
        self._instauser_database_getter = InstaUserDatabaseAdapter(database=database)

    def insert(self, follows):
        """
        This metdod is inserting Follows object to database if it is full and do not exists in the database
        :param follows:
        """
        instauser = self._instauser_database_getter.get(instauser_uid=follows.source_instauser_uid)

        for relation in follows.get_instarelations():
            super().insert(relation)
        for instauser in follows.instausers:
            super().insert(instauser)

        super().update(follows.source_instauser, set_values={'is_loaded': 1})  # Now it is loaded

        if not instauser and not follows.source_instauser.is_instance:
            super().insert(follows.source_instauser)

        #  elif instauser.is_instance:
        #     super().update(follows.instauser, follows.instauser.info)
        # If his friends loaded is_loaded is 1

    def get(self, source_instauser_uid):

        instauser = self._instauser_database_getter.get(source_instauser_uid)

        if instauser and instauser.is_loaded == 0:
            return None

        instausers = []
        query = "SELECT target_instausers.*  FROM instarelations " \
                "INNER JOIN instausers AS target_instausers ON instarelations.target_uid = target_instausers.instauser_uid " + \
                "WHERE instarelations.source_uid = %s" % source_instauser_uid

        rows = self._database.select(query)
        if len(rows) < 1:
            return None

        for row in rows:
            instausers.append(InstaUser(params=row))
        return Follows(from_db=True, instauser=instauser, instausers=instausers)

from Models.InstaUser import InstaUser
from SocialTools.Word import Word
from datetime import datetime

import abc

__author__ = 'guemues'


class SocialBaseObjectDatabaseAdapter(object):

    def __init__(self, database):
        self._database = database

    @abc.abstractmethod
    def get(self, socialbaseobject):
        query = "SELECT * FROM %s WHERE %s = %s" % (socialbaseobject.TABLE_NAME,
                                                    socialbaseobject.UNIQUE_ID,
                                                    socialbaseobject.uid)

        return self._database.select_row(query)

        pass

    def insert(self, socialbaseobject):

        keys_ = []
        values_ = []

        if isinstance(socialbaseobject, InstaUser) \
                and 'gender' in socialbaseobject.vars and 'full_name' in socialbaseobject.info.keys():
            g = self._database.get_gender(name=socialbaseobject.full_name)
            keys_.append('gender')
            values_.append(g)

        for key, value in socialbaseobject.info.items():
            if key in socialbaseobject.vars and value:
                keys_.append(key)
                if isinstance(value, str):
                    value = Word.turkish_utf8(value)
                    values_.append("'" + str(value.encode('utf8'))[2:-1] + "'")
                else:
                    values_.append(str(value))

        keys = "( " + ", ".join(keys_) + " )"
        values = "( " + ", ".join(values_) + " )"

        query = "INSERT INTO %s %s VALUES %s " % (socialbaseobject.TABLE_NAME, keys, values)
        uid = self._database.insert(query)
        if not socialbaseobject.have_uid():
            socialbaseobject.uid = uid

    def update(self, socialbaseobject, set_values):
        set_str = ""

        for set_key, set_value in set_values.items():
            set_value_ = ("\"" + set_value + "\"") if isinstance(set_value, str) else set_value
            if set_key in socialbaseobject.vars and set_value:
                set_str += set_key + " = " + str(set_value_) + ", "

        set_str = set_str[0:-2]

        where_str = socialbaseobject.UNIQUE_ID + ' = ' + str(socialbaseobject.uid)

        query = "UPDATE " + socialbaseobject.TABLE_NAME + " SET " + set_str + " WHERE " + where_str
        self._database.modify(query)

    def delete(self, socialbaseobject, delete_conditions):
        where_str = ""

        for set_key, set_value in delete_conditions.items():
            set_value_ = ("\"" + set_value + "\"") if isinstance(set_value, str) else set_value
            if set_key in socialbaseobject.vars:
                where_str += set_key + " = " + str(set_value_) + " AND "

        where_str = where_str[0:-4]

        query = "DELETE FROM " + socialbaseobject.TABLE_NAME + " WHERE " + where_str
        self._database.modify(query)

        # update end



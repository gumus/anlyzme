from Contollers.Managers.Adapters.SocialBaseObjectDatabaseAdapter import SocialBaseObjectDatabaseAdapter
from Models.InstaUser import InstaUser
from Models.InstaRelation import InstaRelation

__author__ = 'guemues'


class InstaRelationDatabaseAdapter(SocialBaseObjectDatabaseAdapter):

    def __init__(self, database):
        super().__init__(database=database)

    def get(self, source_instauser_uid):

        relations = []

        query = "SELECT *  FROM instarelations WHERE instarelations.source_uid = %s" % source_instauser_uid

        rows = self._database.select(query)

        for row in rows:
            relations.append(InstaRelation(params=row))

        return relations

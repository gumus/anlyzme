from Contollers.Managers.ApiGetters.ApiGetter import ApiGetter
from Models.InstaUser import InstaUser

__author__ = 'guemues'


class InstaUserApiGetter(ApiGetter):

    def __init__(self, instaram_connection, params=None):
        """
            Instagram api getter is responsible for api connection for instagram relations
            like follows and followers informations

        :param instaram_connection: gets InstagramConnection object to reach instagram api
        :param params:
        """
        super().__init__(instaram_connection, params)

    # __init__ end

    def get(self, instauser_uid):
        """
            This methods return the follows of specified instagram uid

        :param instagram_uid: instagram user unique is
        :rtype : follows object
        """
        return InstaUser.change_to_own(user=self._insta.user(instauser_uid))
    # follows end

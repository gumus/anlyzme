from Contollers.Managers.ApiGetters.ApiGetter import ApiGetter
from Models.Follows import Follows
from Models.InstaUser import InstaUser

__author__ = 'guemues'


class InstaRelationApiGetter(ApiGetter):

    def __init__(self, instaram_connection, params=None):
        """
        Instagram api getter is responsible for api connection for instagram relations
        like follows and followers informations

        :param instaram_connection: gets InstagramConnection object to reach instagram api
        :param params:
        """
        super().__init__(instaram_connection, params)

    # __init__ end

    def follows(self, source_instauser):
        """
            This methods return the follows of specified instagram uid

        :param instagram_uid: instagram user unique is
        :rtype : follows object
        """
        followlist = []

        follows_ = self._insta.follows(instauser_uid=source_instauser)

        for user in follows_:
            followlist.append(InstaUser.change_to_own(user))
        return Follows(from_db=False, instauser=source_instauser, instausers=followlist)

    # follows end

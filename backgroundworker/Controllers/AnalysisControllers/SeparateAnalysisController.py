from Contollers.Managers.InstaUserManager import InstaUserManager
from Contollers.AnalysisControllers.AnalysisController import AnalysisController
from Contollers.Managers.CommunityManager import CommunityManager
from Contollers.Managers.NetworkManager import NetworkManager

from Models.MultiNetwork import MultiNetwork

__author__ = 'guemues'


class SeparateAnalysisController(AnalysisController):

    def __init__(self, s3client, database, analysis):
        super().__init__(database=database, analysis=analysis)

        # Relation getter
        self._instauser_community_group_manager = CommunityManager(database=database, s3client=s3client)
        self._instauser_manager = InstaUserManager(database=database, insta_connection=self._insta_connection)
        self._network_manager = NetworkManager(database=database, s3client=s3client)

    def get(self, socialbaseobject):
        raise NotImplementedError

    def do(self):

        network = self._network_manager.get(community_uid=self._analysis.community_uid)

        # Multi network is a composite design pattern.
        instauser_multi_network = MultiNetwork(networkx=network.networkx,
                                               community=network.community,
                                               positions=network.positions)

        self._network_manager.insert(instauser_multi_network)

        # print(community)










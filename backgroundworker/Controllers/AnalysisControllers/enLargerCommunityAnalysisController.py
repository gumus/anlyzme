from Contollers.Managers.InstaUserManager import InstaUserManager
from Contollers.AnalysisControllers.AnalysisController import AnalysisController
from Contollers.Managers.FollowsManager import FollowsManager

__author__ = 'guemues'


class enLargerCommunityAnalysisController(AnalysisController):
    def get(self, socialbaseobject):
        raise NotImplementedError

    def __init__(self, database, analysis):
        super().__init__(database=database, analysis=analysis)

        # Relation getter
        self._user_api_manager = InstaUserManager(database=database, insta_connection=self._insta_connection)
        self._follows_manager = FollowsManager(database=database, insta_connection=self._insta_connection)

    def do(self):
        raise NotImplementedError

from Contollers.InstaConnection import InstaConnection
from Contollers.Managers.Adapters.SocialBaseObjectDatabaseAdapter import SocialBaseObjectDatabaseAdapter

__author__ = 'guemues'


class AnalysisController(SocialBaseObjectDatabaseAdapter):

    def __init__(self, database, analysis):
        super().__init__(database=database)

        self._analysis = analysis
        # All analysis will have api connection
        self._insta_connection = InstaConnection(analysis.access_token)
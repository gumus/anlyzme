from Models.FollowAnalysis import FollowAnalysis
from Models.SeparateAnalysis import SeparateAnalysis
from Models.enLargerCommunityAnalysis import enLargerCommunityAnalysis

from .SeparateAnalysisController import SeparateAnalysisController
from .FollowAnalysisController import FollowAnalysisController
from .enLargerCommunityAnalysisController import enLargerCommunityAnalysisController

__author__ = 'guemues'


class AnalysisFactory(object):

        def __init__(self, database, s3client):
            self._database = database
            self._s3client = s3client

        def get_next_analysis(self):
            query = "SELECT * FROM analysis \
                INNER JOIN users ON users.instauser_uid = analysis.instauser_uid \
                WHERE finished = 0 AND crushed = 0 ORDER BY analysis.created_at ASC LIMIT 1"

            row = self._database.select_row(query)
            if row and "type" in row.keys():
                if row["type"] == 0:
                    return FollowAnalysisController(database=self._database,
                                                    s3client=self._s3client,
                                                    analysis=FollowAnalysis(params=row))
                elif row["type"] == 1:
                    return SeparateAnalysisController(database=self._database,
                                                      s3client=self._s3client,
                                                      analysis=SeparateAnalysis(params=row))
                elif row["type"] == 1000:
                    return enLargerCommunityAnalysisController(database=self._database,
                                                               analysis=enLargerCommunityAnalysis(params=row))
            else:
                return None

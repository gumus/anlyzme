from Contollers.Managers.InstaUserManager import InstaUserManager
from Contollers.AnalysisControllers.AnalysisController import AnalysisController
from Contollers.Managers.FollowsManager import FollowsManager
from Contollers.Managers.NetworkManager import NetworkManager
from Contollers.Managers.CommunityManager import CommunityManager
from Models.Community import Community
from Models.Community import CommunityType
from Models.MultiNetwork import MultiNetwork

__author__ = 'guemues'


class FollowAnalysisController(AnalysisController):
    def __init__(self, s3client, database, analysis):
        super().__init__(database=database, analysis=analysis)

        # Relation getter
        self._community_manager = CommunityManager(database=database, s3client=s3client)
        self._instauser_manager = InstaUserManager(database=database, insta_connection=self._insta_connection)
        self._follows_manager = FollowsManager(database=database, insta_connection=self._insta_connection)
        self._network_manager = NetworkManager(database=database, s3client=s3client)

    def get(self, socialbaseobject):
        raise NotImplementedError

    def do(self):
        all_relations = []

        instauser_main = self._instauser_manager.get_and_insert(self._analysis.instauser_uid)
        print(instauser_main)
        follows = self._follows_manager.get_and_insert(self._analysis.instauser_uid)
        for instauser in follows.instausers:
            follows_ = self._follows_manager.get_and_insert(instauser)
            all_relations += follows_.get_instarelations()

        instauser_network = self._community_manager.create_network(
            Community(from_db=False,  # This is creating an instance of community for all friends of instauser
                      instauser=instauser_main,
                      instausers=follows.instausers,
                      community_type=CommunityType.instauser,
                      params={'source_instauser_uid': instauser_main.uid}
                      ))
        # Multi network is a composite design pattern.
        instauser_multi_network = MultiNetwork(networkx=instauser_network.networkx,
                                               community=instauser_network.community,
                                               positions=instauser_network.positions)

        self._network_manager.insert(instauser_multi_network)

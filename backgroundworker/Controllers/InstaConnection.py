from instagram.client import InstagramAPI
from instagram.bind import InstagramAPIError

__author__ = 'guemues'


class InstaConnection(object):

    def __init__(self, access_token=""):
        """
        Instagram connection holds an Instagram api and responsible for communication
        with Instagram
        :param access_token: is a string to reach instagram
        """
        self._api = None
        self.current_token = None
        self.ready = False
        self.access_token = access_token
        self.refresh()

    def refresh(self, reason=None):
        """

        :param reason:
        """
        self.ready = False
        if reason == 429:
            self.current_token.rate_limit_ex()

        while not self.ready:
            self._api = InstagramAPI(access_token=self.access_token, client_secret='13f9198a3a4045c0b4b2bd8bbdcb738f')
            self.ready = True

    def follows(self, instauser_uid):
        """

        :param instauser_uid:
        :return: list of insta user
        """
        follows_ = []
        try:
            follows_, next_ = self._api.user_follows(instauser_uid, count=100)
            while next_:
                more_follows, next_ = self._api.user_follows(with_next_url=next_)
                follows_.extend(more_follows)
        except InstagramAPIError as error:
            print(error)

        return follows_

    def user(self, instauser_uid):
        """

        :param instauser_uid:
        :return: list of insta user
        """
        user = None
        try:
            user = self._api.user(instauser_uid)
        except InstagramAPIError as error:
            print(error)

        return user

from enum import Enum
from Contollers.AnalysisControllers.AnalysisFactory import AnalysisFactory

import threading
import time

__author__ = 'guemues'


class Status(Enum):
    Ready = 1
    InProcess = 2
    Idling = 3
    Crushed = 4
    Finished = 5


class AnalysisThread(threading.Thread):

    def __init__(self, database, s3client, thread_id, analysis_controller=None):
        threading.Thread.__init__(self)
        self._factory = AnalysisFactory(database=database, s3client=s3client)

        self._database = database
        self._thread_id = thread_id
        self._ready = True
        self._status = Status.Ready
        self._analysis_controller = analysis_controller

    def run(self):
        if self._analysis_controller:
            self._status = Status.InProcess
            self._analysis_controller.do()
            self.stop()

        while self._ready:

            #  If this thread created for a general purpose
            if not self._analysis_controller:
                analysis = self._factory.get_next_analysis()

                self._status = Status.InProcess
                if analysis:
                    analysis.do()
                self._status = Status.Idling

                time.sleep(1)

            #  If this thread is special

    def stop(self):
        self._status = Status.Finished
        self._ready = False

    @property
    def status(self):
        return self._status

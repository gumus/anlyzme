#!/usr/bin/env python3.4
# -*- coding: iso-8859-15 -*-
from InstagramController.AnalysisFactory import AnalysisFactory
from InstagramController.InstaConnection import InstaConnection
from InstagramController.InstaUserApiGetter import InstaUserApiGetter
from InstagramController.SocialBase import SocialBase
from InstagramModel.FollowAnalysis import FollowAnalysis

MY_ID = 1537676451

database_connection = SocialBase.get_instance()

__author__ = 'guemues'

analysis = AnalysisFactory(database=database_connection).get_next_analysis()
analysis.do()

<?php
/**
 * Created by PhpStorm.
 * User: guemues
 * Date: 13/11/15
 * Time: 20:59
 */

;
use Illuminate\Database\Seeder;


class InstaUsersTableSeeder extends Seeder{

    public function run(){

        DB::table('instausers')->delete();

        $instausers = array(
            array(
                'instauser_uid'     => 1537676451,
                'username'          => 'gumusorcun',
                'profile_picture'   => 'http://scontent.cdninstagram.com/hphotos-xat1/t51.2885-19/11123664_1415854112063923_191677110_a.jpg',
                'full_name'         =>  'Orcun Gumus',
                'is_public'         => 1
            )
        );
        DB::table('instausers')->insert($instausers);


    }


}
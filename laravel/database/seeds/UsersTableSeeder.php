<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $users = array(
            array(
                'instauser_uid'     => 1537676451,
                'access_token'          => '1537676451.031f760.67be2b2a5b344327bc147b46c53662a7',
            )
        );
        DB::table('users')->insert($users);

    }
}

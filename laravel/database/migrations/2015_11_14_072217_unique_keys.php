<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UniqueKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table)
        {

        });
        Schema::table('analysis', function($table)
        {
            //$table->dropPrimary('analysis_uid');
        });
        Schema::table('instausers', function($table)
        {
            $table->dropPrimary('instauser_uid');
        });
        Schema::table('instauser_community', function($table)
        {
            //$table->dropPrimary('instauser_community_uid');
        });
        Schema::table('instauser_commnity_relation', function($table)
        {
            //$table->dropPrimary('instauser_commnity_relation_uid');
        });
        Schema::table('instarelations', function($table)
        {
            //$table->dropPrimary('instarelations_uid');
        });
        Schema::table('names', function($table)
        {
            $table->dropPrimary('uname');
        });
    }
}

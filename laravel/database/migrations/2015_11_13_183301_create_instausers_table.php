<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstausersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instausers', function (Blueprint $table) {
            $table->bigInteger('instauser_uid');
            $table->integer('is_public')->nullable();
            $table->integer('total_follows')->nullable();
            $table->integer('total_followers')->nullable();
            $table->integer('total_media')->nullable();
            $table->integer('is_loaded')->default(0);
            $table->string('username')->nullable();
            $table->string('profile_picture')->nullable();
            $table->string('full_name');
            $table->char('gender',1)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('instausers');
    }
}

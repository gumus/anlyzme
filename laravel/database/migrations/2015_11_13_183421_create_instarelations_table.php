<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstarelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instarelations', function (Blueprint $table) {
            $table->bigIncrements('instarelations_uid');
            $table->bigInteger('source_uid');
            $table->bigInteger('target_uid');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('instarelations');
    }
}

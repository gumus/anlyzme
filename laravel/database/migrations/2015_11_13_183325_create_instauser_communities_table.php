<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstauserCommunitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instauser_communities', function (Blueprint $table) {
            $table->increments('instauser_community_uid');
            $table->bigInteger('source_instauser_uid');
            $table->double('clustering_coef',8,7);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('instauser_communities');
    }
}

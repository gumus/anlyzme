<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstauserCommunityRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instauser_community_relations', function (Blueprint $table) {
            $table->bigIncrements('instauser_commnity_relation_uid');
            $table->bigInteger('instauser_uid');
            $table->integer('instauser_community_uid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('instauser_community_relations');
    }
}

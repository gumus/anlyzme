function see_all() {
    $('.sigma-node, .sigma-edge, .sigma-label').each(function () {
        unmute(this);
    });
}


function mute(node) {
    if (!~node.getAttribute('class').search(/muted/))
        node.setAttributeNS(null, 'class', node.getAttribute('class') + ' muted');
}

function unmute(node) {
    node.setAttributeNS(null, 'class', node.getAttribute('class').replace(/(\s|^)muted(\s|$)/g, '$2'));
}

path = window.location.pathname

$.getJSON( path + "/JSON", function(json) {
    s = new sigma({
        graph: json,
        settings: {
            hideEdgesOnMove: true,
            enableHovering: false,
            zoomMax: 6,
            scalingMode : 'outside'
        }
    });
    s.addRenderer({
        id: 'main',
        type: 'svg',
        container: document.getElementById('graph-container'),
        freeStyle: true
    });
    s.refresh();
    $('.sigma-node').click(function() {
        var node = this;
        $.ajax({
            method: "GET",
            url: '/profile/sidebar/' + $(this).attr('data-node-id') + '/'
        })
            .done(function( msg ) {
                $("#profile-sidebar").html(msg);
                //Total neigbours number
                var neighbors = s.graph.neighborhood($(node).attr('data-node-id'));
                //Total friend number
                var total = $('.sigma-node').length;

                //Ratio of knowing same persons
                var ratio = (neighbors.nodes.length / total);
                $("#more_info").html(friend_level(ratio));

                //text = "<table class=\"table table-responsive\">"
                //neighbors.nodes.forEach(function(node) { text += "<tr><td>" + node.id + "</td></tr>";});
                //text += "</table>"
                //$("#common_friends").html(text);
            });
        // Muting
        $('.sigma-node, .sigma-edge, .sigma-label').each(function() {
            mute(this);
        });

        // Unmuting neighbors
        var neighbors = s.graph.neighborhood($(this).attr('data-node-id'));
        neighbors.nodes.forEach(function(node) {
            unmute($('[data-node-id="' + node.id + '"]')[0]);
            unmute($('[data-label-target="' + node.id + '"]')[0]);
        });

        neighbors.edges.forEach(function(edge) {
            unmute($('[data-edge-id="' + edge.id + '"]')[0]);
        });


    });

});/**
 * Created by guemues on 24/11/15.
 */

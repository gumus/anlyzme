<?php
/**
 * Created by PhpStorm.
 * User: guemues
 * Date: 24/11/15
 * Time: 14:51
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Community extends Model
{
    use \Stevebauman\EloquentTable\TableTrait;


    protected $primaryKey = 'community_uid';
    protected $table = 'communities';
    protected $fillable = ['source_instauser_uid', 'clustering_coef', 'created_at', 'updated_at'];
}
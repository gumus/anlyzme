<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\InstaUser;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

use MetzWeb\Instagram\Instagram;

class SuccessPageController extends Controller
{
    public function processLogin(Request $request){
        $result = "default";

        if($request->has('code')){
            $instagram = new Instagram(array(
                'apiKey' => '031f760e5c164a508275b3ce0a4051eb',
                'apiSecret' => '13f9198a3a4045c0b4b2bd8bbdcb738f',
                'apiCallback' => 'http://analyzeforme.elasticbeanstalk.com/success'
            ));

            $code = $request->input('code');

            $data = $instagram->getOAuthToken($code);

            $user = User::find($data->user->id);
            if( is_null($user) ) {
                Auth::loginUsingId($data->user->id);
                $user->access_token = $data->access_token;
                $user->save();

            }
            else {
                User::updateOrCreate(['instauser_uid' => $data->user->id,], [
                    'instauser_uid' => $data->user->id,
                    'access_token' => $data->access_token,
                ]);

                InstaUser::updateOrCreate(['instauser_uid' => $data->user->id,],[

                    'username' => $data->access_token,
                    'profile_picture' => $data->user->profile_picture,
                    'full_name' => $data->user->full_name,
                ]);
            }
            return view('test', ['data' => $data]);
        }


    }
}

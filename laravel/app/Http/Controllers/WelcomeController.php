<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Config;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use MetzWeb\Instagram\Instagram;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;


class WelcomeController extends Controller
{
    public function index(){
        // initialize class
        $instagram = new Instagram(array(
            'apiKey' => '031f760e5c164a508275b3ce0a4051eb',
            'apiSecret' => '13f9198a3a4045c0b4b2bd8bbdcb738f',
            'apiCallback' => 'http://analyzeforme.elasticbeanstalk.com/success' // must point to success.php
        ));

        if (Config::get('app_env') == 'aws') {
            return view('welcome', ['login_url' => $instagram->getLoginUrl()]);
        } else{
            return "Welcome Mr Developper; are you sure that you must be here? Try /{id}";
        }
    }

    public function developerIndex($id){

        $user = User::find($id);
        if (!is_null($user)) {
            Auth::login($user);

            if (Auth::check()) {
                $user = Auth::user();
                return redirect()->route('myGraph', $user->instauser_uid);
            }
        }
        else {
            return "Burada olman sacma bence. I think you are making mistakes man. Fuck off.
             Said by orcun from " . Config::get('app.app_env') ;

        }

    }
}

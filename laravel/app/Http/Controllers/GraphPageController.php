<?php

namespace App\Http\Controllers;

use AWS;
use DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class GraphPageController extends Controller
{
    public function index($id){

        return view('myGraph', ['user' => Auth::user() ]);
    }
    public function getInstauserJSON($id){

        $community = DB::table('communities')
            ->select('community_uid')
            ->where('source_instauser_uid', '=', $id)
            ->whereNull('source_community_uid')
            ->first();

        $community_uid = $community->community_uid;

        $s3 = AWS::createClient('s3');

        $a = $community_uid;
        $a++;

        return $s3->getObject(array(
            'Bucket' => 'analyzeforme-graphs',
            'Key'    => "$id/$community_uid.json"
        ))['Body'];
    }
}

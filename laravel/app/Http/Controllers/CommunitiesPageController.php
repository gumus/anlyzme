<?php

namespace App\Http\Controllers;

use DB;
use App\InstaUser;
use App\Community;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;


class CommunitiesPageController extends Controller
{
    public function index($id){
        $communities_persons = array();

        $communities = Community::where('source_instauser_uid', '=', $id)->get();
        foreach ($communities as $community){
            $communities_persons[] = DB::table('community_relations')
                                        ->join('instausers', 'community_relations.instauser_uid', '=' ,'instausers.instauser_uid')
                                        ->select('instausers.username', 'instausers.full_name')
                                        ->where('community_relations.community_uid', '=', $community->community_uid )
                                        ->get();
        }
        return View::make('main')
            ->with('user',  Auth::user())
            ->with('communities_persons', $communities_persons);
    }
}

<?php

namespace App\Http\Controllers;

use AWS;
use App\User;
use App\InstaUser;
use App\InstaUserCommunity;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProfileInformationController extends Controller
{
    public function getSideBar($id){

        $user = InstaUser::find($id);
        return view('partials.profile', ['user' => $user]);

    }
}

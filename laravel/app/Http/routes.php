<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');
Route::get('/{id}', 'WelcomeController@developerIndex');
Route::get('/success', 'SuccessPageController@processLogin');

Route::get('/home', function(){
    if(Auth::guest()) {
        return Redirect::to('/');

    }
    return 'Welcome Home';
});

Route::get('/profile/sidebar/{id}/', 'ProfileInformationController@getSideBar');
Route::get('/myGraph/{id}', array('as' => 'myGraph', 'uses' => 'GraphPageController@index' ) );
Route::get('/myGraph/{id}/JSON','GraphPageController@getInstauserJSON');
Route::get('/myCommunities/{id}/', 'CommunitiesPageController@index');
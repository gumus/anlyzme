<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstaUser extends Model
{
    use \Stevebauman\EloquentTable\TableTrait;


    protected $primaryKey = 'instauser_uid';
    protected $table = 'instausers';
    protected $fillable = ['instauser_uid', 'profile_picture', 'username', 'full_name', 'total_follows', 'total_followers'];
}

<?php
/**
 * Created by PhpStorm.
 * User: guemues
 * Date: 24/11/15
 * Time: 14:51
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommunityRelation extends Model
{
    use \Stevebauman\EloquentTable\TableTrait;

    protected $primaryKey = 'community_relation_uid';
    protected $table = 'community_relations';
    protected $fillable = ['instauser_uid', 'community_uid', 'created_at', 'updated_at'];
}
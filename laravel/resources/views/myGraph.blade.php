@extends('layouts.logged')

@section('content')
        <div id="container">
            <div class="graph-area">
                <div  id="graph-container" ></div>
            </div>

            <div class="sidebar">
                <div id="profile-sidebar">
                </div>
            </div>
        </div>
@stop

@section('scripts')
    @include('includes.sigma')
@stop

@section('styles')
    {!!  HTML::style('css/graph.css') !!}
@stop
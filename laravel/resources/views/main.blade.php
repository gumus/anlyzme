@extends('layouts.logged')

@section('content')
    <?
            $i = 0;
            foreach ($communities_persons as $community) {

                if ($i % 12 == 0)
                    echo '<div class="row" >';
                echo '<div class="col-md-4" >';
                echo '<table class="table-condensed table" >';
                echo '<caption><b>A community</b></caption>';
                foreach ($community as $instauser) {
                    echo '<tr>' ;
                    echo '<td>' ;
                    echo $instauser->username;
                    echo '</td>' ;
                    echo '</tr>' ;
                }
                echo '</table>';
                echo '</div>';
                if ($i % 12 == 8)
                    echo '</div>';

                $i+=4;
            }
    ?>

@stop
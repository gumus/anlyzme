@extends('layouts.master')

@section('navbar')

    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a href="{{ url('/') }}" class="navbar-brand">analyzefor.me</a>
        </div>
        <ul class="nav  navbar-nav">
            <li class="{{ Request::segment(1) === 'myGraph' ? 'active' : '' }}">

                <a href="{{ url("/myGraph/$user->instauser_uid") }}">My Graph</a>
            </li>

            <li class="{{ Request::segment(1) === 'myCommunities' ? 'active' : '' }}">
                <a href="{{ url("myCommunities/$user->instauser_uid") }}">My Communities</a>
            </li>

            <li class="{{ Request::segment(1) === 'myAnalyzes' ? 'active' : '' }}">
                <a href="{{ url("myAnalyzes/$user->instauser_uid") }}">My Analyzes</a>
            </li>


        </ul>
        <ul class="nav  navbar-nav pull-right">
            <li class="{{ Request::segment(1) === 'about' ? 'active' : '' }}">
                <a href="{{ url('about') }}">About</a>
            </li>
            <li class="{{ Request::segment(1) === 'myProfile' ? 'active' : '' }}">
                <a href="{{ url('myProfile') }}">My Profile</a>
            </li>
            <li>
                <a href="{{ url('logout') }}">Logout</a>
            </li>

        </ul>
    </nav>
@stop


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Social Analyzer</title>

    <!-- Bootstrap -->
    {!!  HTML::style('css/bootstrap.css') !!}
    {!!  HTML::style('css/style.css') !!}
    @yield('styles')


</head>
<body>
    <div id="wrapper">
        @yield('navbar')

        @yield('content')
    </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    {!!  HTML::script('js/jquery-2.1.4.min.js') !!}
            <!-- Include all compiled plugins (below), or include individual files as needed -->
    {!!  HTML::script('js/bootstrap.js') !!}

    @yield('scripts')


</body>
</html>

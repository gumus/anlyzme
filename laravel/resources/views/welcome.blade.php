<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Social Analyzer</title>

    <!-- Bootstrap -->
    {!!  HTML::style('css/bootstrap.css') !!}
    {!!  HTML::style('css/style.css') !!}
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <![endif]-->

    <style>
        .login {
            display: block;
            font-size: 20px;
            font-weight: bold;
        }
    </style>


</head>
<body>
<div class="container">
    <header class="clearfix">
        <h1>analyzefor.me<span>See yourself in socaial network</span></h1>
    </header>
    <div class="main">
        <ul class="grid">
            <li><img src="assets/instagram-big.png" alt="Instagram logo"></li>
            <li>
                <a href="{{ $login_url }}" class="login" >» Login with Instagram</a>
                <h4>Use your Instagram account to login.</h4> Remember this app is not ready. This is only for alpha tests.

            </li>
        </ul>
        <!-- GitHub project -->
        <footer>
            <p>created in Bilkent, see this blog for more details.<br></p>
        </footer>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
{!!  HTML::script('js/jquery-2.1.4.min.js') !!}
<!-- Include all compiled plugins (below), or include individual files as needed -->
{!!  HTML::script('js/bootstrap.js') !!}
</body>
</html>

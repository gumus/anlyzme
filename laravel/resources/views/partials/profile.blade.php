<div class="profile-userpic">
    <img id="profile_picture" src="{{ $user->profile_picture  }}" class="img-responsive" alt="">
</div>
<!-- END SIDEBAR USERPIC -->
<!-- SIDEBAR USER TITLE -->
<div class="profile-usertitle">
    <div class="profile-usertitle-name">
        {{ $user->full_name  }}
    </div>

    <div class="profile-usertitle-job">
        {{ $user->username  }}
    </div>
    <div class="container">

        <div class = "row">
            <div class = "col-sm-2"></div>
            <div class = "col-sm-4">
                <h4><span class="label label-primary">{{ $user->total_follows  }} follows</span></h4>
            </div>
            <div class = "col-sm-4">
                <h4><span class="label label-primary">{{ $user->total_followers  }} followers</span></h4>
            </div>
            <div class = "col-sm-2"></div>
        </div>
        <div class = "row" style="margin-top: 10px"></div>
        <div class="row">
            <div class = "col-sm-1"></div>
            <div class = "col-sm-10">
                <div class="alert alert-info" >
                    <span id="more_info">
                    </span>
                </div>
            </div>
            <div class = "col-sm-1"></div>
        </div>
        <div class="row">
            <div class = "col-sm-1"></div>
            <div class = "col-sm-10">
                <div class="alert alert-primary" role="alert">I think</div>
            </div>
            <div class = "col-sm-1"></div>
        </div>
        <div id="common_friends" ></div>
        <div><button class="btn btn-warning" onclick="see_all();" > I want to see all edges</div>
    </div>

</div>

